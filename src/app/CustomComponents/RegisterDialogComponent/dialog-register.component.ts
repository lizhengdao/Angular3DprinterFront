import { Component, OnInit } from '@angular/core';
import {UserService} from "../../Services/User.Services";
import {User} from "../../Models/User";
import {Router} from "@angular/router";
@Component({
  selector: 'app-dialog-register',
  templateUrl: './dialog-register.component.html',
  styleUrls: ['./dialog-register.component.css'],
  providers:[UserService]
})
export class DialogRegisterComponent implements OnInit {
//inputs
  public username:string;
  public firstname:string;
  public lastname:string;
  public phonenumber:number;
  public email:string;
  public  Password:string;
  public  rePassword:string;
  myStyle: object = {};
  myParams: object = {};
  width: number = 100;
  height: number = 100;
  //messages
  pwcheck:string = '';
  emailcheck:string ='';
  repwcheck :string='';

  //loader
  loading:Boolean = false;

  constructor(private _userservice:UserService, private router:Router) {}

  ngOnInit() {
  }
  register(){


    this.loading = true;
    let user = new User();
    user.username = this.username;
    user.password = this.Password;
    user.email = this.email;

    if (this.checkpassword(this.Password) !== "okeyy" || this.validateEmail(this.email) == false || this.reckpassword() == false) {


    } else {


      this._userservice.Registration(user).subscribe(
        (data)=>{
          this.loading= false;
          alert("registration done !! ");
          this.router.navigate(['/login']);


        }


      )
      ;
    }

  }





  /*form validation functions */

  reckpassword():boolean {
    if ( this.rePassword.length ===0 ){
      this.repwcheck = '<p> </p>';
      return false;
    }
    if (this.Password !== this.rePassword) {
      this.repwcheck = '<p class="red"> passwords does not match </p>';
      return false;
    } else {
      this.repwcheck = '<p  class="green" > Great !! password match !! </p>';
      return true;
    }
  }

  ckpassword() {
    if(this.Password.length==0){
      this.pwcheck = '<p> </p>';
    }else {
      if (this.checkpassword(this.Password) !== "okeyy") {
        this.pwcheck = '<p class="red">' + this.checkpassword(this.Password) + '</p>';
      } else {
        this.pwcheck = '<p class="green">Great your password is valid  !!</p>';
      }
    }
  }

  checkpassword(str:string):string {
    if (str.length < 6 && str.length>0) {

      return ("minimum length should be 6 !! ");
    } else if (str.length > 50) {

      return ("password too long !! ");
    } else if (str.search(/\d/) == -1) {

      return (" the password should contain numbers and letters please enter numbers !! ");
    } else if (str.search(/[a-zA-Z]/) == -1) {

      return ("the password should contain numbers and letters please enter letters !! ");
    } else if (str.search(/[^a-zA-Z0-9\!\@\#\$\%\^\&\*\(\)\_\+\.\,\;\:]/) != -1) {

      return ("bad charts !! ");
    }else {
      return "okeyy";
    }

  }


  validateEmail(email:string):boolean {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  validateE():boolean {

    if (this.email.length > 2) {
      if (this.validateEmail(this.email)) {

        this.emailcheck = '<p class="green"> Great !! email is  valid </p>';
        return true;
      } else {

        this.emailcheck = '<p class="red">Error !! please enter a valid email ' +
          'the email should have this format xxx@sthg.yy</p>';
        return false;
      }

    }else {
      this.emailcheck = '<p class="green"> </p>';

    }
  }
}
