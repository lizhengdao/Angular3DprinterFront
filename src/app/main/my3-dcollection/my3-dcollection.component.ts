import {Component, HostListener, OnInit} from '@angular/core';

import {FileUploader, FileUploaderOptions} from 'ng2-file-upload';
import {UserService} from "../../Services/User.Services";
import {My3dmodelService} from "../../Services/my3dmodel.service";
import {Observable} from "rxjs/Observable";
import {my3DmodelsResponse} from "../../interfaces/3dmodelsResponse";
import {MatDialog} from "@angular/material";
import {DialogloginComponent} from "../../CustomComponents/LoginDialogComponent/Dialoglogin.component";
import {UploadStldialogComponentComponent} from "../../CustomComponents/UploadSTLDialogComponent/upload-stldialog-component.component";

@Component({
  selector: 'app-my3-dcollection',
  templateUrl: './my3-dcollection.component.html',
  styleUrls: ['./my3-dcollection.component.css'],
  providers: [UserService, My3dmodelService],

})
export class My3DcollectionComponent implements OnInit {

  public loading: Boolean = false;
  public models = [];
  public isLoaded: Boolean=true;

  constructor(private  userservice: UserService, private  my3dmodelservice: My3dmodelService, public dialog: MatDialog) {

  }

  openDialog(): void {

    let dialogRef = this.dialog.open(UploadStldialogComponentComponent, {
      position: {
        top: '80px',
      },
      width: '450px',
      height: '450px'

    });

  }
  @HostListener('document:UPDATE_MODELS_LIST', ['$event'])
  someAction(event) {
    this.ngOnInit();

  }

  ngOnInit(): void {


    this.my3dmodelservice.GetAllModels(this.userservice.GetCurrentUser()._id).subscribe(
      (data) => {

        this.models = data;

      },
      (error)=>{

      },
      () => {
        this.isLoaded=true;
      }
    );


  }



}
