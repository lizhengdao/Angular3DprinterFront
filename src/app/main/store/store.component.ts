import { Component, OnInit } from '@angular/core';
import { My3dmodelService } from '../../Services/my3dmodel.service';
import { UserService } from '../../Services/User.Services';
import { MatSnackBar } from '@angular/material';
import { MessageSnackBarComponent } from '../../CustomComponents/message-snack-bar/message-snack-bar.component';
import { SearchPipe } from '../../pipes/search.pipe';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.css'],
  providers: [My3dmodelService,UserService,SearchPipe],
  
})
export class StoreComponent implements OnInit {
  public search:any = '';
  public loading: Boolean = false;
  public models = [];
  public isLoaded: Boolean=true;
  constructor( private  my3dmodelservice: My3dmodelService, private SnackBar:MatSnackBar,public userService:UserService,private SearchPipe: SearchPipe) { }

  ngOnInit() {


    this.my3dmodelservice.GetModels().subscribe(
      (data) => {
       console.log(data);
        this.models = data;

      },
      (error)=>{

      },
      () => {
        this.isLoaded=true;
      }
    );

  }
  AddToMyCollection(idmodel:string):void{

   this.my3dmodelservice.AddToMyCollection(idmodel,this.userService.GetCurrentUser()._id).subscribe(
     ()=>{
      this.SnackBar.openFromComponent(MessageSnackBarComponent,{
        duration: 3000,
        data: { content: "Model successfully shared !!  " }
      }); 
     }
   )


  }

}
