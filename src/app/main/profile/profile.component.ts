import { Component, OnInit } from '@angular/core';
import {UserService} from "../../Services/User.Services";
import {User} from "../../Models/User";
import {FileUploader, FileUploaderOptions} from 'ng2-file-upload';
import {Router} from "@angular/router";
import {AuthService} from "../../Services/auth.service";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [UserService]
})
export class ProfileComponent implements OnInit {
  public  username:string = " ";
  public  firstname:string = " ";
  public  lastname:string = " ";
  public  address:string = " ";
  public  email:string = " ";
  public  usernameinput:string = " ";
  public  firstnameinput:string = " ";
  public  lastnameinput:string = " ";
  public  addressinput:string = " ";
  public  emailinput:string = " ";
  public  profilecompleted:boolean=false ;
  public  loading:boolean=false;
  public  profilepicUrl="http://localhost:3000/uploads/avatar.png";
  public uploader:FileUploader ;
  public  loadimg:Boolean = false;

  constructor(private  userservice:UserService, private router:Router,private authService:AuthService) {



  }


  ngOnInit() {
    if(this.firstname === " " || this.lastname=== "" || this.address==="" ){
      this.profilecompleted = true;
    }
    if(this.authService.isLoggedOut()==true){
      this.router.navigate(['/login']);
      let body = document.getElementsByTagName('body')[0];
      body.setAttribute("style","background:");
    }

    if(this.userservice.GetCurrentUser()!= null){
      this.loadData();

      this.uploader =  new FileUploader(
        {url:'http://localhost:3000/api/updateprofilepic/'+this.userservice.GetCurrentUser()._id,
          headers: [{
            name:'x-access-token',
            value:localStorage.getItem("id_token")

          }]


      }
      );

      this.uploader.onAfterAddingFile = (fileItem) => {
        this.loadimg = true;
        console.log('complete');
        this.uploader.uploadItem(fileItem);


      };
      this.uploader.onCompleteItem = ()=>{
         this.loadimg=false;
        this.loadData();
      }
    }else {
      this.router.navigate(['/login']);
      let body = document.getElementsByTagName('body')[0];
      body.setAttribute("style","background:");
    }



  }
  updateprofile(){
    let user = new User();

    user.username = this.usernameinput;
    user.firstname = this.firstnameinput;
    user.lastname = this.lastnameinput;
    user.email = this.emailinput;
    user.address = this.addressinput;
    this.loading=true;

    this.userservice.UpdateProfile(user,this.userservice.GetCurrentUser()._id).subscribe(
      (data)=>{

        this.loadData();

        this.loading =false;
      }
    )

  }


  loadData(){

  this.userservice.GetUserById(this.userservice.GetCurrentUser()._id ).subscribe(
    (data)=>{
     console.log(data);

      this.username = data.username;
      this.email = data.email;
      this.firstname = data.firstname;
      this.lastname = data.lastname;
      this.address = data.address;
      this.usernameinput = data.username;
      this.emailinput = data.email;
      this.firstnameinput = data.firstname;
      this.lastnameinput = data.lastname;
      this.addressinput = data.address;
      if(data.profilepicUrl === ""){
        this.profilepicUrl = "http://localhost:3000/uploads/avatar.png";
      }else {
        this.profilepicUrl = "http://localhost:3000/uploads/"+data.profilepicUrl;
      }

    }
  );




}



}
