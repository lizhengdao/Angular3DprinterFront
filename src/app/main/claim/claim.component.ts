import { Component, OnInit } from '@angular/core';
import { Claim } from '../../Models/Claim';
import { ClaimService } from '../../Services/claim.service';
import { UserService } from '../../Services/User.Services';
import { MatSnackBar } from '@angular/material';
import { MessageSnackBarComponent } from '../../CustomComponents/message-snack-bar/message-snack-bar.component';

@Component({
  selector: 'app-claim',
  templateUrl: './claim.component.html',
  styleUrls: ['./claim.component.css'],
  providers: [ClaimService,UserService]
})
export class ClaimComponent implements OnInit {
 public title:string="" ; 
 public problem:string="" ; 
 public content:string="" ; 
 public loading:boolean = false; 
  constructor(public userService:UserService,  private SnackBar:MatSnackBar,public claimService:ClaimService) { }

  ngOnInit() {
  }
  send(){
    this.loading = true; 
  let claim = new Claim();
  claim.Description = this.content ; 
  claim.Title = this.title; 
  claim.ProblemType = this.problem ; 
  this.claimService.AddClaim(claim,this.userService.GetCurrentUser()._id).subscribe(
    ()=>{
   this.loading = false; 
   this.SnackBar.openFromComponent(MessageSnackBarComponent,{
    duration: 3000,
    data: { content: "Your Claim was successfully send !!  " }

  });  
   this.title="" ; 
   this.problem="" ; 
   this.content="" ;
  
    }
  )


  }

}
