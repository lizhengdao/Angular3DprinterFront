import { Injectable } from '@angular/core';
import {UserResponse} from "../interfaces/UserResponse";
import {HttpClient} from "@angular/common/http";

import {Observable} from "rxjs/Observable";

import {my3Dmodel} from "../Models/my3Dmodel";

@Injectable()
export class My3dmodelService {

  constructor(private _http:HttpClient) { }
  public GetAllModels(userid:string):Observable<my3Dmodel[]>{

    return this._http.get<my3Dmodel[]>("http://localhost:3000/api/models/"+userid).map(
      (res)=>res
    );
  }
  public DeleteModel(idmodel:string){
    return this._http.delete<my3Dmodel[]>("http://localhost:3000/api/models/delete/"+idmodel).map(
      (res)=>res
    );
  }
  public GetModels():Observable<my3Dmodel[]>{

    return this._http.get<my3Dmodel[]>("http://localhost:3000/api/models");
  }
  public AddToMyCollection(idmodel:string,iduser:string){
    return this._http.get("http://localhost:3000/api/addToMyCollection/"+iduser+"/"+idmodel);
  }

}
