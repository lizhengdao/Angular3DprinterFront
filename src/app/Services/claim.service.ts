import { Injectable } from '@angular/core';
import { Claim } from '../Models/Claim';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ClaimService {

  constructor(public http:HttpClient) { }

  public AddClaim(claim:Claim,userid:string){
        
    return this.http.post("http://localhost:3000/api/claim/"+userid,claim); 


  }
}
