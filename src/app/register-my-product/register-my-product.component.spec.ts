import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterMyProductComponent } from './register-my-product.component';

describe('RegisterMyProductComponent', () => {
  let component: RegisterMyProductComponent;
  let fixture: ComponentFixture<RegisterMyProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterMyProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterMyProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
